import { showModal } from "./modal";

export function showWinnerModal(fighter) {
	// call showModal function
  const winnerInfo = {
    title: 'Winner',
    bodyElement: fighter.name
  }
  
  showModal(winnerInfo);
}
